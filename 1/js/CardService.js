import URL from './urls.js'
import LoginService from "./LoginService.js";

let cards = JSON.parse(localStorage.getItem('cards')) || [];

class cardService {
    async createCard() {
        const visit = {
            title: "Визит к кардиологу",
            description: 'Плановый визит',
            doctor: "Cardiologist",
            bp: "24",
            age: 23,
            weight: 70
        };

        let response = await fetch(URL.cardsURL, {
            method: 'POST',
            headers: {
                ...LoginService.getAuthorizationHeader(),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(visit)
        });
        if (response.ok) {
            let body = await response.json();
            let card = {
                id: body.id,
                content: body.content
            }
            cards.push(card);
            localStorage.setItem('cards', JSON.stringify(cards));
            console.log('Карточка успешно создана')
        } else {
            throw new Error('Все плохо');
        }
    }

    async getCards() {

        let response = await fetch(URL.cardsURL, {
            method: 'GET',
            headers: {
                ...LoginService.getAuthorizationHeader(),
            },
        });
        let allCards = await response.json();
        console.log('Все которые есть на cервере', allCards);
        // console.log('те которые у меня в массиве начали после создания сохраняться:', cards);
        if (response.ok) {
            alert('Карточки получены')
        } else {
            throw new Error();
        }
    }

    async deleteCard(id) {
        let response = await fetch(`${URL.cardsURL}/${id}`, {
            method: 'DELETE',
            headers: {
                ...LoginService.getAuthorizationHeader(),
            },
        });
        if (response.ok) {
            let index = cards.findIndex(elem => elem.id === id)
            cards.splice(index, 1);
            localStorage.setItem('cards', JSON.stringify(cards));
            alert('Карточка удалена');
        } else {
            throw new Error();
        }
    }
}

export default new cardService();


