import URL from './urls.js'

class LoginService {
    async login(email, password) {
        const data = {email, password};

        let response = await fetch(URL.login, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        let token = await response.text();
        if (response.ok) {
            saveToken(token);
        } else {
            throw new Error(token);
        }

        function saveToken(token) {
            document.cookie = `token=${token}`;
        }
    }

    getToken() {
        return getCookie('token');

        function getCookie(name) {
            let matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }

    getAuthorizationHeader() {
        return {
            'Authorization': `Bearer ${this.getToken()}`
        };
    }
}

export default new LoginService();



