import React from 'react'
import {connect} from 'react-redux'


export class Toolbar extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return <div>
            Toolbar {this.props.page.year}
        </div>
    }

}

const mapStateToProps = store => ({
    page: store.page
})

export default connect(mapStateToProps)(Toolbar)