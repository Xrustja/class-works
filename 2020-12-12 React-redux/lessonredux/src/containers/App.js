import './App.css';
import React from 'react';
import {connect} from 'react-redux'
import {User} from '../components/User'
import {Page} from '../components/Page'
import {setYear} from '../actions/PageActions'

class App extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        const {user, page, setYearAction} = this.props
        console.log('Props =>', this.props) // посмотрим, что же у нас в store?

        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">Мой топ фото</h1>
                </header>
                <User name={user.name} />
                <Page photos={page.photos} year={page.year} setYear={setYearAction} />
            </div>
        )
    }
}

const mapStateToProps = store => {
    console.log('STORE =>', store) // посмотрим, что же у нас в store?
    return {
        user: store.user,
        page: store.page
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setYearAction: year => dispatch(setYear(year))
    }
}



// в наш компонент App, с помощью connect(mapStateToProps)
export default connect(mapStateToProps,mapDispatchToProps)(App)
