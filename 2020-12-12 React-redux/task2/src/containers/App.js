import './App.css';
import React from 'react'
import Toolbar, {ToolBar} from '../../src/components/ToolBar'
import ResultCalculation from "../components/ResultCalculation";
import Main from "../components/Main";
import {connect} from "react-redux";


export default function App() {
    return (
        <div className="App">
            <Toolbar/>
            <Main/>
            <ResultCalculation result={100}/>
        </div>
    );
}

//
// const mapStateToProps = store => {
//     console.log(store);
//     return {
//         figures: store.figures
//     }
// }
//
// export default connect(mapStateToProps)(App)
//
