import {combineReducers} from "redux";

import {triangleReducer} from './triangle'
import {squareReducer} from './square'
import {rectangleReducer} from './rectangle'
import {appReducer} from './app'


export const rootReducer = combineReducers({
    rectangle: rectangleReducer,
    triangle: triangleReducer,
    square: squareReducer,
    app:appReducer,
})