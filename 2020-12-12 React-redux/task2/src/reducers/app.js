import {TYPE_RECTANGLE, TYPE_SQUARE, TYPE_TRIANGLE} from "../components/Figures";

const initialState = {
    currentFigure: TYPE_TRIANGLE,
    figures: [{title: 'Трикутник', type: TYPE_TRIANGLE},
        {title: 'Прямокутник', type: TYPE_RECTANGLE},
        {title: 'Квадрат', type: TYPE_SQUARE}]
}

export function appReducer(state = initialState) {
    return state
}