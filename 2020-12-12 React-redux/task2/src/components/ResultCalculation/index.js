import React, {Component} from 'react'

export default class ResultCalculation extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        const {result} = this.props;
        return (
            <div className="result-wrapper">
                <p className="result">
                    Result: {this.props.result}
                </p>
            </div>
        )
    }

}