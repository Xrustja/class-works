import React, {Component} from 'react'
import {Rectangle, Square, Triangle, TYPE_RECTANGLE, TYPE_SQUARE} from "../Figures";


export default class Main extends Component {
    render() {
        return (
            <div className="main">
                {
                    this.props.figure === 'TYPE_RECTANGLE'
                        ? <Rectangle/>
                        : this.props.figure === 'TYPE_SQUARE'
                            ? <Square/>
                            : <Triangle/>
                    }
            </div>
        )
    }

}