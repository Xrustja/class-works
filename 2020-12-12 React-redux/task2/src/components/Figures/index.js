import Rectangle from './Rectangle'
import Triangle from './Triangle'
import Square from './Square'

const TYPE_TRIANGLE = 'TRIANGLE';
const TYPE_RECTANGLE = 'RECTANGLE';
const TYPE_SQUARE = 'SQUARE'

export {
    Rectangle,
    Triangle,
    Square,
    TYPE_TRIANGLE,
    TYPE_RECTANGLE,
    TYPE_SQUARE
};