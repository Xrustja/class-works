import React, {Component} from 'react'
import PropTypes from 'prop-types'
import FigureSelector from "../FigureSelector";
import {TYPE_TRIANGLE, TYPE_RECTANGLE, TYPE_SQUARE} from "../Figures";
import {connect} from 'react-redux'

export default class ToolBar extends Component{

    render() {
        console.log('PROPS in ToolBar ', this.props)

        return (
            <div className="toolbar-wrapper">
                <FigureSelector />
            </div>
        )
    }
}
//
// const mapStateToProps = store => {
//     console.log(store);
//     return {
//         figures: store.figures
//     }
// }
//
// export default connect(mapStateToProps)(ToolBar)
//
