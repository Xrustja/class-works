import React from 'react'
import PropTypes from 'prop-types'
import {connect} from "react-redux";


class FigureSelector extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        console.log('PROPS in FigureSelector ', this.props)
        const {figures} = this.props
        return (
            <div>
                <select name="select-field" id="select-field">
                    {figures.map(figure => <option
                        key={figure.type}
                        value={figure.title}>
                        {figure.title}
                    </option>)
                    }
                </select>
            </div>
        )
    }
}


FigureSelector.propTypes = {
    figures: PropTypes.array.isRequired
}

const mapStateToProps = store => {
    console.log(store);
    return {
        figures: store.app.figures
    }
}

export default connect(mapStateToProps)(FigureSelector)