import React from 'react'
import PropTypes from 'prop-types'

export default class Button extends React.Component {
    constructor(props) {
        super(props);
        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick() {
        this.props.onClick()

    }

    render() {
        return (<button onClick={this.onBtnClick}> {this.props.title}</button>)
    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    title: PropTypes.string
}