import React from 'react'
import Button from "../Buttons";
import './Modal.css'
import PropTypes from 'prop-types'


class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this)
    }

    onClose() {
        this.props.onClose()
    }

    render() {

        const styles = {
            display: this.props.show ? 'block' : 'none'
        }

        return (
            <div className='modal' style={styles}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            {this.props.title}
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        <div className="modal-footer">
                            <Button onClick={this.onClose} title='X'/>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    show: PropTypes.bool,
    onClose: PropTypes.func,
}
export default Modal;