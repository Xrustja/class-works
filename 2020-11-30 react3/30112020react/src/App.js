import logo from './logo.svg';
import './App.css';
import React from 'react'
import Button from "./components/Buttons";
import Modal from "./components/Modal";

class App extends React.Component {
    constructor (props) {
        super (props)

        this.state = {
            isDialogShow:false
        }
        this.onDialogShow = this.onDialogShow.bind(this);
        this.onDialogClose = this.onDialogClose.bind(this);
    }

    onDialogShow () {
        this.setState({isDialogShow:true})
    }
    onDialogClose () {
        this.setState ({isDialogShow:false})
    }

  render () {
       return (
           <div className="application-wrapper">
               <Button onClick={this.onDialogShow} title='Button'/>
               <Modal title='Test modal'
                      show={this.state.isDialogShow}
                      onClose={this.onDialogClose}><h1>Hello</h1><h1>Hello</h1><h1>Hello</h1> </Modal>
           </div>
       )
    };
}

export default App;
