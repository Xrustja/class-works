class Loader {
    constructor() {
        this._status = null;
        this.images = ['https://drugoy.com.ua/uploads/blog/579fc-slide-1960x857-07.jpg',
            'https://img5.goodfon.ru/wallpaper/nbig/1/c3/priroda-les-reka-porogi-peizazh-gory-kamni.jpg',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTYleNj3tzO6Ff9xMHpuNvVjUxCyUc8sCDX_Q&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTF7ciEplKllhwlIbGXYnpH4qQ7wTkYm12CQg&usqp=CAU']
    }

    loadAll() {
        const imgArr = [];
        this.images.forEach(function (src) {
            imgArr.push(this.load(src))
        }.bind(this));
        return Promise.all(imgArr)
    }
    

    load(url) {
        return new Promise((resolve, reject) => {
            let img = document.createElement('img');
            img.src = url;
            img.onload = function () {
                resolve(img)
            }
            img.onerror = function (e) {
                reject(e);
            }
        });
    }
}

const loader = new Loader();
loader.loadAll()
    .then((images) => {
        console.log(images);
        for (const img of images) {
            document.body.appendChild(img)
        }
    })
    .catch((e) => {
        console.error('Error--->', e);
    });

// const loader = new Loader();
// loader.load('https://drugoy.com.ua/uploads/blog/579fc-slide-1960x857-07.jpg')
//     .then((el) =>
//         document.body.appendChild(el))
//     .catch((e) => {
//         console.log('Error --->', e);
//     })
