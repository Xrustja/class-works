// import axios from "./node_modules/axios/lib/axios.js";

const visit = {
    title: "Визит к кардиологу",
    description: 'Плановый визит',
    doctor: "Cardiologist",
    bp: "24",
    age: 23,
    weight: 70
};
document.getElementById('btn-obj').onclick = () => {
    function createObj(visit) {
        // return new Promise((resolve, reject) => {
        axios
            .post('https://ajax.test-danit.com/api/cards', visit, {
                headers: {
                    'Authorization': 'Bearer 00a8fe06-36f9-4674-9e11-d0f1b5d7f568'
                }
            })
            .then(response => console.log(response));
    }

    console.log(createObj(visit));
}