import './App.css';
import React from 'react'
import LoadingBar from "./components/LoadingBar";

function App() {

    const seconds = 10;

    return (
        <div className="App">
            <input type="text"/>
            <LoadingBar seconds={seconds}/>
        </div>
    );
}

export default App;
