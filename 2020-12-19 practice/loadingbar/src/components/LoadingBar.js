import React, {useEffect, useState} from 'react'


export default function LoadingBar(props) {
    const [barWidth, setBarWidth] = useState(0);


    function hope(newArr) {
        newArr.map(item => setInterval(
            function loading() {
                setBarWidth(item);
                }
            , 10000))
    }

    useEffect(() => {
        let arr = []
        for (let i = 1; i <= 422; i += 1) {
            arr.push(i);
        }
        let newArray = arr.slice();
        console.log(arr);
        hope(newArray)
    }, [])
    return (

        <div>
            {console.log('inRender=> ')}
            <h1>Loading for {props.seconds}... </h1>
            <div className='loadingContainer'>
                <div style={{
                    width: `${barWidth}px`,
                    backgroundColor: "lightskyblue",
                    height: "58px",
                    border: "none"
                }}>

                </div>
            </div>
        </div>
    )
}