import {ACTION_LOGIN} from "../actions/loginAction";
import LocalStorageService from '../../services/localStorage.service';
const storage = LocalStorageService.getItem();
const initialState = {
    isAuth: !!(storage && storage.token)
}


export function appReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_LOGIN:
            return {
                ...state, isAuth: true
            }
        default:
            return state;
    }
}