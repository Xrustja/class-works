export const ACTION_LOGIN = 'ACTION_LOGIN';

export function loginAction({login,password}) {
    return {
        type: ACTION_LOGIN,
        payload: {login, password}
    }
}