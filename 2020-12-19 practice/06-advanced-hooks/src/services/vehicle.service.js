import {request} from "./core"
import VehicleModel from './models/vehicle.model'

class VehicleService {
    static async getVehicles() {
        const {data} = await request(
            {url: "/vehicles"}
        )
        return data.map(item => new VehicleModel(item));
    }
}

export default VehicleService;