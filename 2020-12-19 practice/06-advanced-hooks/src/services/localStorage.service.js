class LocalStorageService {
    constructor() {
        this.key = 'ourAppKey'
    }

    setItem(value) {
        return localStorage.setItem(this.key, JSON.stringify(value))
    }

    getItem() {
        try {
        return JSON.parse(localStorage.getItem(this.key))
        } catch (e) {
            return {}
        }
    }
}

export default new LocalStorageService();