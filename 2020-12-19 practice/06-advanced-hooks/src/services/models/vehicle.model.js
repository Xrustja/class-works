export default class VehicleModel {
    constructor({id, name, manufacturer, model, vehicleClass, costInCredits}) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.model = model;
        this.vehicleClass = vehicleClass;
        this.costInCredits = costInCredits;
    }
}