import {Button, Checkbox, Form, Input} from 'antd';
import './Authorize.css'
import {useDispatch, useSelector} from "react-redux";
import {loginAction} from "../../../redux/actions/loginAction";
import {useState} from 'react'
import userService from '../../../services/user.service'
import LocalStorageServiceEntity from '../../../services/localStorage.service'
import {Redirect, useHistory} from "react-router-dom";

function AuthorizePage() {

    const loginDispatch = useDispatch();
    const [logining, setLogining] = useState(false);
    const [error, setError] = useState('');
    const history = useHistory();
    const auth = useSelector((state) => state.isAuth)

    const layout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };
    const tailLayout = {
        wrapperCol: {offset: 8, span: 16},
    };

    const onFinish = async (values) => {
        try {
            setLogining(true);
            const user = await userService.authorize(values);
            console.log('USER =>', user);
            LocalStorageServiceEntity.setItem(user);
            loginDispatch(loginAction(values));
            history.push('/')

        } catch (e) {
            setError(new Error(e))
        } finally {
            setLogining(false);
        }
    };


    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };
    return (

        <div className="login-page">
            {auth ? <Redirect to={'/'}/> :
                <Form
                    {...layout}
                    onFinish={onFinish}
                    initialValues={{remember: true}}
                    name="Log-In"
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        help={"Enter email"}
                        name="email"
                        label="E-mail"
                        rules={[
                            {
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                            {
                                required: true,
                                message: 'Please input your E-mail!',
                            },
                        ]}
                    >
                        <Input placeholder="Email"/>
                    </Form.Item>

                    <Form.Item
                        help={"Enter password"}
                        hasFeedback={true}
                        label="Password"
                        name="password"
                        rules={[{required: true, message: 'Please input your password!'}]}
                    >
                        <Input.Password placeholder="Password"/>
                    </Form.Item>

                    <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit" disabled={logining}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            }
        </div>
    )
}

export default AuthorizePage;