import VehicleService from "../../../services/vehicle.service";
import {useEffect, useState} from 'react'


function VehiclePage() {
    const [vehicles, setVehicles] = useState([]);
    useEffect(async () => {
            setVehicles(await VehicleService.getVehicles());
        },
        []
    )

    return (
        <div>
            <ul>
                {vehicles.map(vehicle => <li key={vehicle.id}>{vehicle.name}</li>)}
            </ul>
        </div>
    )
}

export default VehiclePage;