import './App.css';
import {connect, useDispatch, useSelector} from "react-redux";


function AppHook(props) {
    console.log('App props:', props);

    const hookDispatch = useDispatch();
    const loveTarget = useSelector(state => state.love.loveTarget);

    const click = () => {
        hookDispatch({
            type: 'CHANGE',
            newTarget: 'Sergey'
        });
    };

    const click2 = () => {
        hookDispatch(dispatch => {
            dispatch({type: 'CHANGE', newTarget: '...loading...'});

            setTimeout(() => {
                dispatch({type: 'CHANGE', newTarget: 'Sergey'});
            }, 2000);
        });
    }

    return (
        <div className="App">
            I love {loveTarget}!<br/>
            <button onClick={click}>Change target</button>
            <button onClick={click2}>Change target with delay</button>
        </div>
    );
}

export default AppHook;
