const initialState = {
    loveTarget: 'Christina'
};

export default function reducer(state = initialState, action) {
    console.log('Reducer: ', state, action);
    switch (action.type) {
        case 'CHANGE':
            if (state.loveTarget !== action.newTarget) {
                return {
                    loveTarget: action.newTarget
                }
            } else {
                return initialState;
            }
        case 'THUNK_START':
            return {
                loveTarget: action.newTarget
            }
        default:
            return state;
    }
}