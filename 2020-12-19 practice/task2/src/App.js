import './App.css';
import {connect} from "react-redux";


function App(props) {
    console.log('App props:', props);

    const click = () => {
        props.changeLove();
    };

    const click2 = () => {
        props.dispatch(dispatch => {
            dispatch({type: 'CHANGE', newTarget: '...loading...'});

            setTimeout(() => {
                dispatch({type: 'CHANGE', newTarget: 'Sergey'});
            }, 2000);
        });
    }

    return (
        <div className="App">
            I love {props.target}!<br/>
            <button onClick={click}>Change target</button>
            <button onClick={click2}>Change target with delay</button>
        </div>
    );
}

export default connect(state => {
    console.log('App state:', state);
    return {
        qwe: 'qwe',
        target: state.love.loveTarget
    }
}, dispatch => {
    return {
        changeLove: () => dispatch({
            type: 'CHANGE',
            newTarget: 'Sergey'
        }),
        dispatch: dispatch
    }
})(App);
