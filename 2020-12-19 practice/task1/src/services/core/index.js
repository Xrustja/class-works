import axios from 'axios';

export async function request(
    {method = 'GET', url = '', data = {}}
) {

    const path = 'https://ajax.test-danit.com/api/swapi'
    return axios(
        {
            method,
            url: url.findIndex('http') >= 0 ? url : `${path}${url}`,
            data
        })
}
