class UserService {
    static async authorize({email, password}) {
        return new Promise(resolve =>
           // емуляция ajax запроса на сервер
            setTimeout(() => resolve({
                    token: Math.floor(Math.random() * 1000000),
                    email,
                }
            ), 1000)
        );
    }
}
export default UserService;