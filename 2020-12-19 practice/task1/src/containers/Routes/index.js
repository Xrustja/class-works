import React from 'react';
import {Switch, Route} from "react-router-dom";
import LoginPage from '../../components/pages/Login'
import HomePage from '../../components/pages/Home'
import VehiclePage from '../../components/pages/Vehicle'

function Routes() {
    return (
        <main>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route  path='/login' component={LoginPage}/>
                <Route path='/vehicles' component={VehiclePage}/>
            </Switch>
        </main>
    );
}

export default Routes;