import './App.css';
import {Link} from "react-router-dom";
import Routes from "./Routes";
import {Menu} from 'antd';
import {MailOutlined} from '@ant-design/icons';
import {useState} from 'react'
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'

function App() {
    const [current, setCurrent] = useState();
    const handleClick = e => {
        setCurrent({current: e.key})
    }
    return (
        <div>
            <header>
                <Menu onClick={handleClick}
                      selectedKeys={[current]}
                      mode="horizontal">
                    <Menu.Item key="Home" icon={<MailOutlined/>}>
                        <Link to="/">Home</Link>
                    </Menu.Item>
                    <Menu.Item key="Login" icon={<MailOutlined/>}>
                        <Link to="/login">Login</Link>
                    </Menu.Item>
                    <Menu.Item key="vehicles" icon={<MailOutlined/>}>
                        <Link to="/vehicles">Vehicles</Link>
                    </Menu.Item>
                </Menu>
                {/*<nav>*/}
                {/*    <ul>*/}
                {/*        <li><Link to="/">Home</Link></li>*/}
                {/*        <li><Link to="/vehicles">Vehicles</Link></li>*/}
                {/*        <li><Link to="/login">Login</Link></li>*/}
                {/*    </ul>*/}
                {/*</nav>*/}
            </header>
            <Routes/>
        </div>

    )
}

export default App

//
// function App() {
//     return (
//         <div>
//             <header>
//                 <ul>
//                     <li>
//                         <Link to="/">Home</Link>
//                     </li>
//
//                     <li>
//                         <Link to="/vehicle">Vehicle</Link>
//                     </li>
//                     <li>
//                         <Link to="/login">Login</Link>
//                     </li>
//
//                 </ul>
//             </header>
//             <Routes/>
//         </div>
//
//     )
// }