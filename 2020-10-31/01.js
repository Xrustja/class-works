document.addEventListener('DOMContentLoaded', function () {
    const button = document.getElementById('testConnection');

    button.onclick = onSendRequestBtnClick;
});

/**
 * @desc Execute HTTP request
 //  **/
// function onSendRequestBtnClick() {
//     alert('Hello');
// }

function onSendRequestBtnClick() {
    const xhttp = new XMLHttpRequest();

    xhttp.open('GET', 'https://swapi.dev/api/people/1/', true)
    xhttp.onreadystatechange = function () {
        try {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                console.log(xhttp.response);
            }
        } catch (e) {
            console.log('Error',e);
        }
    }
    console.log('ready state ->', xhttp);



xhttp.send()

}
