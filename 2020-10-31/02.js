class Ajax {
    constructor() {
    }

    static getRequest(url) {
        return new Promise((resolve, reject) => {
            const xhttp = new XMLHttpRequest();

            xhttp.open('GET', url)
            xhttp.onreadystatechange = function () {
                try {
                    if (xhttp.readyState === 4 && xhttp.status === 200) {
                        resolve(JSON.parse(xhttp.responseText));
                    } else if (xhttp.readyState === 4 && xhttp.status > 200) {
                        resolve(xhttp.responseText);
                    }
                } catch (e) {
                    reject(e);
                }
            };
            xhttp.onerror = reject;
            xhttp.onabort = reject;
            xhttp.send();
        });
    }
}

class Person {
    constructor(props) {
        // const {name, height, mass, hair_color} = props;
        // console.log('Props => ',props);
        for (let key in props) {
            this[key] = props[key];
        }
    }

    toString() {
        return `Person: <ul>${Object.entries(this)
            .map(([key, value]) => `<li><strong>${key}</strong>: ${value}</li>`)
            .join('')}</ul>`;
    }
}