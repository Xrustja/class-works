document.addEventListener('DOMContentLoaded', function () {
    const ul = document.createElement("ul");
    ul.innerHTML = '<li>Loading...</li>';
    ul.setAttribute('id', 'ul-wrapper')
    ul.onclick = onPersonBtnClick;

    document.body.append(ul)
    Ajax.getRequest('https://swapi.dev/api/people/')
        .then(function (response) {
            ul.innerHTML = response.results
                .map(function(item, index) {
                    return `<li>${new Person(item)}
                    <button>Delete</button>
                    </li>`
                })
                .join('');
            console.log(response);
        })

});

function onPersonBtnClick(event) {
    const element = event.target;
    if (element.tagName === 'BUTTON'){
        element.closest('li').remove();
    }
}