document.addEventListener('DOMContentLoaded', function () {
    const ul = document.createElement('ul');
    ul.innerHTML = '<li>Loading...</li>';
    ul.setAttribute('id', 'ul-wrapper')
    document.body.append(ul);

    fetch('https://swapi.dev/api/films/')
        .then(response => response.json())
        .then(response => {
            ul.innerHTML = '';
            response.results
                .map(film => new Film(film))
                .forEach(film => film.toDOM(ul));
        });
});

class Film {
    constructor({episode_id, title, opening_crawl, characters}) {
        this.episode_id = episode_id;
        this.title = title;
        this.opening_crawl = opening_crawl;
        this.characters = characters;
    }

    toDOM(parent) {
        let li = document.createElement('li');
        li.innerHTML = 'Film:';
        let ul = document.createElement('ul');
        li.append(ul);
        for (let key in this) {
            if (this.hasOwnProperty(key)) {
                let value = this[key];
                let innerLi = document.createElement('li');
                if (!Array.isArray(value)) {
                    innerLi.innerHTML = `<strong>${key}</strong>:${value}`;
                } else {
                    innerLi.innerHTML = '<em>Loading...</em>';
                    Promise.all(value.map(character => fetch(character).then(response => response.json())))
                        .then(response => {
                            innerLi.innerHTML = `<strong>${key}</strong>:`;
                            let charactersUl = document.createElement('ul');
                            innerLi.append(charactersUl);
                            for (let el of response) {
                                let characterLi = document.createElement('li');
                                characterLi.innerHTML = el.name;
                                charactersUl.append(characterLi);
                            }
                        });
                }
                ul.append(innerLi);
            }
        }
        parent.append(li);
    }
}


