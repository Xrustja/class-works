function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
}

/**
 * *@desc Повне ім'я
 * @returns {string}
 */
Person.prototype.getFullName = function () {
    return `${this.firstname} ${this.lastname}`
}

function User(email, password, firstname, lastname) {
    this.email = email;
    this.password = password;
    Person.call(this, firstname, lastname)
}
// Не правильно!!!!
// User.prototype = Person.prototype;
// правильно!!!!
User.prototype = Object.create(Person.prototype);
// User.prototype.getFullName = function () {
//     return `${Person.prototype.getFullName(this)} !!!!!}`
// };

const person = new Person('Max', 'Bilyk');
const user = new User(
    'ivan.ivanov@gmail.com',
    '123456789',
    'Ivan',
    'Ivanov'
);