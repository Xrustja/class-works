import logo from './logo.svg';
import React from 'react';
import './App.css';
import UserList from "./components/UserList";

function App() {


    return (<div className='app'>
            <UserList/>
        </div>
    );
}

export default App;
