import moment from "moment";


class User {
    constructor({
                    id,
                    first_name,
                    last_name,
                    middle_name,
                    birthday,
                    description,
                    city,
                    department
                }
    ) {
        this.id = id;
        this.firstName = first_name;
        this.lastName = last_name;
        this.middleName = middle_name;
        this._birthday = birthday;
        this.description = description;
        this.city = city;
        this.department = department;

    }

    get name() {
        return `${this.firstName} ${this.lastName} ${this.middleName}`
    }

    get birthday() {
        if (this._birthday) {
            return moment(this._birthday).format('DD.MM.YYYY')
        }
        return 'N/A'
    }
}
export default User