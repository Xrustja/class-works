import users from '../data/users.json' ;
import {userModel} from './models'

class UserService {
    static loadUsers() {
        return users.map(user => new userModel(user));
    }
}

export default UserService;