import React from 'react';
import UserCard from "../UserCard";
import users from '../../data/users.json' ;
import userService from "../../services/userService"
import UserService from "../../services/userService";

export default function UserList() {
const users = UserService.loadUsers();
    return (
        <div className='user-list card-group'>
            {
                users.map(user => <UserCard user={user} key={user.id}/>)
            }
        </div>
    );
}