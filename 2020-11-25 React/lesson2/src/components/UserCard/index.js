import React from 'react';


export default function UserCard(props) {
       const {user} = props;
    return (
        <div className='card'>
            <div className='card-body'>
                <h5 className='card-title'> {`${user.name}`} </h5>
                <p className='card-text'> {user.description} </p>
                <p className='card-text'><small className='text-muted'> {user.birthday} </small></p>
                <p className='card-text'><small className='text-muted'> {user.city} </small></p>
            </div>
        </div>
    )
}
//
//
// return (
//     <div className='card'>
//         <div className='card-body'>
//             <h5 className='card-title'> {`${user.first_name} ${user."last_name"} ${user."middle_name"}`} </h5>
//             <p className='card-text'> {user.description} </p>
//             <p className='card-text'><small className='text-muted'> {user.description} </small></p>
//             <p className='card-text'><small className='text-muted'> {user.city} </small></p>
//         </div>
//     </div>
// )