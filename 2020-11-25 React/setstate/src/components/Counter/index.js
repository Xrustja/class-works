import React, {Component} from 'react'
import './Counter.css'
class Counter extends Component{
    constructor(props) {
        super(props);
        const {init=0} = this.props
        this.state = {
            counter: init
        };
        this.onBtnClick = this.onBtnClick.bind(this)  // ЯВНО забайндить ПРОСТО ТАК НУЖНО ДЕЛАТЬ
    }
    onBtnClick () {
        // console.log('Hello',this)
        this.setState({
            counter: this.state.counter + 1
        });
    }
    render() {
        console.log('ihihjihih',this.props, this.state)
        return (
            <div className='counter-wrapper'>
                <div>Timer: {this.props.time}</div>
                <div className='counter-stat'>{this.state.counter}</div>
                <button onClick={this.onBtnClick} className='btn-click'>Click me</button>
                {/*<button onClick={() => this.onBtnClick()} className='btn-click'>Click me</button>*/}
            </div>
        )
    }
}
export default Counter