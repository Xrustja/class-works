import React from 'react';
import Menu from '../../common/Menu'

/**
 *
 * @descr This is super cool first component
 **/


function Header() {
    return <div className='header'>
        <section className='logo'>Logo wrapper</section>
        <section className='menu'>
            <Menu/>
        </section>
        <section>
            User menu
        </section>
        This is header!!!</div>
}

export default Header;