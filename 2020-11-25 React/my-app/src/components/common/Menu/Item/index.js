import React from 'react';

export default function MenuItem(props) {
    const {url, title,style} = props;

    return <li className={style === 'light' ? 'light-item' : 'dark-item'}><a href={url}>{title}</a></li>
}