import React from 'react';
import MenuItem from "./Item";
import './Menu.css'

// class Menu extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {items: []};
//     }
//
//     componentDidMount() {
//         this.setState({
//             items: [
//                 {url: '0', title: 'Menu item'},
//                 {url: '1', title: 'Menu item 1'},
//                 {url: '2', title: 'Menu item 2'},
//                 {url: '3', title: 'Menu item 3'},
//                 {url: '4', title: 'Menu item 4'},
//             ]
//         })
//     }
//
//     render() {
//         return (
//             <div className='menu-wrapper'>
//                 <ul>
//                     {
//                         this.state.items.map(
//                             item => <MenuItem key={item.url}
//                                               url={item.url}
//                                               style={style}
//                                               title={item.title}/>
//                         )
//                     }
//                 </ul>
//             </div>
//         )
//     }
// }


export default function Menu(props) {
    const {style = 'light'} = props;
    const items = [
        {url: '0', title: 'Menu item'},
        {url: '1', title: 'Menu item 1'},
        {url: '2', title: 'Menu item 2'},
        {url: '3', title: 'Menu item 3'},
        {url: '4', title: 'Menu item 4'},
    ]

    return (
        <div className='menu-wrapper'>
            <ul>
                {
                    items.map(
                        item => <MenuItem key={item.url}
                                          url={item.url}
                                          style={style}
                                          title={item.title}/>
                    )
                }
            </ul>
        </div>
    )
}


 // export default Menu;