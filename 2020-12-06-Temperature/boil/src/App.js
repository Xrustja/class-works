import React from 'react'
import './App.css';
import Input from "./components/Input";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            scale: ''
        }

        this.onCelsiusChange = this.onCelsiusChange.bind(this);
        this.onFahrenheitChange = this.onFahrenheitChange.bind(this);
        this.checkBoiling = this.checkBoiling.bind(this);
        this.convertTemperature = this.convertTemperature.bind(this);
        this.toFahrenheit = this.toFahrenheit.bind(this);
        this.toCelsius = this.toCelsius.bind(this);

    }

    toFahrenheit(val) {
        return val * 9 / 5 + 32
    }

    toCelsius(val) {
        return (val - 32) * 5 / 9
    }

    convertTemperature(value, convert) {
        if (value === '') {
            return ''
        } else {
            const convertedTemp = convert(value);
            console.log('Math.round', Math.round(convertedTemp * 1000) / 1000);
            return (Math.round(convertedTemp * 1000) / 1000);
        }
    }


    onCelsiusChange(temperature) {
        this.setState({
            value: temperature,
            scale: 'c'
        })

    }

    onFahrenheitChange(temperature) {
        this.setState({
            value: temperature,
            scale: 'f'
        })
    }

    checkBoiling(t) {
        if (t === '') {
            return ''
        } else {
            return (t < 100) ? 'Water does not boil' : 'Water is boiling'
        }
    }

    render() {
        const celsius = this.state.scale === 'c' ? this.state.value : this.convertTemperature(this.state.value, this.toCelsius);
        const fahrenheit = this.state.scale === 'f' ? this.state.value : this.convertTemperature(this.state.value, this.toFahrenheit);
        return (
            <div className="App">
                <Input type='Cesium' onChange={this.onCelsiusChange} value={celsius}/>
                <Input type='Fahrenheit' onChange={this.onFahrenheitChange} value={fahrenheit}/>
                <p><strong>{this.checkBoiling(celsius)}</strong></p>
            </div>
        );
    }
}

export default App;
