import React from 'react';

class Input extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        const tempValue = e.target.value;
        if (!isNaN(tempValue)) {
            return this.props.onChange(tempValue);
        } else {
           return  alert('Enter NUMBER');
        }
    }

    render() {
        return (
            <fieldset>
                <legend>Enter degrees in {this.props.type}</legend>
                <input value={this.props.value} onChange={this.onChange}>
                </input>
            </fieldset>
        )
    }
}

export default Input;