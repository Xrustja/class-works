import Base, {MAX_TIMEOUT} from './BaseService'

export class PlanetService extends Base {
    constructor() {
        super();
    }

    static async getPlanets() {
        return PlanetService.requestGet('http https://swapi.dev/api/planets/');
    }
}