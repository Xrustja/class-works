import axios from 'axios';


export const MAX_TIMEOUT = 500;
export default class BaseService {
    constructor() {

    }
    /**
     * @description
     **/
    static async requestGet(url) {
        return axios.get(url);
    }
}
