export class Planet {
    constructor(props) {
        const {
            climate,
            edited,
            films,
            gravity,
            name
        } = props;
        // for (let prop in props) {
        //     this[prop] = props[prop];
        // }
        this.edited = edited;
        this.climate = climate;
        this.films = films;
        this.gravity = gravity;
        this.name = name;

    }
}