class Flowers {
    constructor(country, days_to_expire, price) {
        this.country = country
        this.days_to_expire = days_to_expire
        this.price = price
        Flowers.counter++
    }

    test() {

    }
}

Flowers.counter = 0;


class Roses extends Flowers {
    constructor() {
        super('Ukraine', 15, 45);
    }
}

class Tulips extends Flowers {
    constructor() {
        super('Holland', 10, 35);
    }
}

class Pions extends Flowers {
    constructor() {
        super('France', 12, 50);
    }
}

class Test {
}

let bouquet1 = [new Roses(), new Roses(), new Roses()];
let bouquet2 = [new Roses(), new Pions(), new Tulips(), new Roses()];
let bouquet3 = [new Roses(), new Roses(), new Tulips(), new Roses()];

function priceCounter(bouquet) {
    return bouquet.reduce((acumulator, elem) => {
        return acumulator + elem.price;
    }, 0)
}

console.log(priceCounter(bouquet1));
console.log(priceCounter(bouquet2));
console.dir(Flowers);
console.dir(bouquet1)
