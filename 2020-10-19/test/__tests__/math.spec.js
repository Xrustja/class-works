const math = require( '../math');
describe ("Sum for 2 numbers", () => {
    test("it should sum 2 and 3 and return 5", () => {
        expect(math.sum(2,3)).toEqual(5);
    });
});