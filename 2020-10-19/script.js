const destinationPrice = [
    {dest: 'Pechersk', index: 5.5},
    {dest: 'Obolon', index: 2.5},
    {dest: 'Troeshchyna', index: 0.5},
];
const materialPrices = [
    {title: 'brick', index: 10},
    {title: 'concrete', index: 2},
    {title: 'blocks', index: 3.5},
];
const floorPrices = [
    {min: 0, max: 5, index: 5},
    {min: 6, max: 9, index: 9},
    {min: 10, max: 15, index: 16},
    {min: 16, max: 30, index: 20}
];

function Building({
                      floorNumbers = 5,
                      address = null,
                      developer = null,
                      initialPrice = 1000,
                      parking = false,
                      heatingType = null,
                      buildingType = null,
                      materials = null,
                      yearOfRelease = null
                  }) {
    this.floorNumbers = floorNumbers;
    this.address = address;
    this.developer = developer;
    this.initialPrice = initialPrice;
    this.parking = parking;
    this.heatingType = heatingType;
    this.buildingType = buildingType;
    this.materials = materials;
    this.yearOfRelease = yearOfRelease;
}

Building.prototype.calcPrice = function (floor) {
    if (!this.address) {
        throw new Error('Address is required!!!')
    }
    if (!this.materials) {
        throw new Error('Material is required!!!')
    }
    console.log(this);

    const material = materialPrices
        .find(function (material) {
            console.log(this);
            return material.title === this.materials;
        }.bind(this)) || {index: 1};

    const address = destinationPrice
        .find(function (address) {
            return address.dest === this.address;
        }.bind(this)) || {index: 1};

    const floorPrice = floorPrices
        .find(function (floorItem) {
            return floor >= floorItem.min && floor <= floorItem.max
        }) || {index: 1};
    return this.initialPrice * (
        material.index + address.index + floorPrice.index
    );
};


const b1 = new Building({
    floorNumbers: 20,
    address: 'Obolon',
    materials: 'blocks'
});
const b2 = new Building({
    floorNumbers: 15,
    address: 'Obolon',
    materials: 'brick'
});


// b1.calcPrice(19)