export const SET_YEAR = 'page/SET_YEAR'
export const RANDOM_YEAR = 'page/RANDOM_YEAR'


export function setYear(year) {
    return {
        type: 'SET_YEAR',
        payload: year,
    }

}