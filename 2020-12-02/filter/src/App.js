import React from 'react'
import './App.css';
import {FilmService} from './services';
import FilmList from "./components/FilmList";
import InputFilmFilter from "./components/InputFilmFilter";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            films: [],
            filter: '',
            loading: true
        }
        this.onFilmDelete = this.onFilmDelete.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);
    }

    async componentDidMount() {
        const {data: films} = await FilmService.getFilms();
        this.setState({films, loading: false});
    }

    onFilmDelete(id) {
        console.log("Parent =>>", id);
        // const {films} = this.state;
        // const index = films.findIndex(item => item.id === id);
        // if (index >= 0) {
        //     films.splice(index, 1);
        //     this.setState({films});
        // }
        this.setState((prevState) => {
            const {films} = prevState;
            const index = films.findIndex(film => film.id === id);

            if (index >= 0) {
                films.splice(index, 1);
            }
            return {
                ...prevState,
                films
            }
        });

        // this.setState((oldState) => {
        //     const {films} = oldState;
        //     const index = films.findIndex(item => item.id === id);
        //     if (index >= 0) {
        //         films.splice(index, 1);
        //         return {films};
        //     } else {
        //         return oldState;
        //     }
        // })
    }

    onFilterChange(filter) {
        this.setState({filter});
    }

    render() {
        console.log('Render', this.state.films, this.state.filter)
        const films = this.state.filter
            ? this.state.films.filter(film =>
                film.name.indexOf(this.state.filter) >= 0)
            : this.state.films

        return (

            <div className="App">
                <InputFilmFilter onChange={this.onFilterChange} disabled={this.state.loading}/>
                <FilmList items={films} onDelete={this.onFilmDelete}/>
            </div>
        );
    }
}

export default App;
