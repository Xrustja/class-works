import React from 'react';
import PropTypes from 'prop-types';


class FilmList extends React.Component {
    constructor(props) {
        super(props);
        // this.onDeleteBtnClick = this.onDeleteBtnClick.bind(this);
    }

    onDeleteBtnClick(filmId) {
        this.props.onDelete(filmId);
    }

    render() {
        return (
            <div className='film-list'>
                <ul> {this.props.items.map(item => <li
                    key={item.id}> Episode {item.episodeId}:<strong> {item.name}</strong>
                    <button onClick={() => this.onDeleteBtnClick(item.id)}>Delete</button>
                </li>)}

                </ul>
            </div>
        )
    }
}

FilmList.propTypes = {
    items: PropTypes.array,
    onDelete: PropTypes.func,
    filter:PropTypes.string
}

export default FilmList;

