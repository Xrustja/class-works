import React from 'react'
import PropTypes from "prop-types";
import FilmList from "../FilmList";

class InputFilmFilter extends React.Component {
    constructor(props) {
        super(props);
        this.onFilterChange = this.onFilterChange.bind(this);
    }

    onFilterChange(e) {
        this.props.onChange(e.target.value);
    };

    render() {
        return (
            <div className='input-filter'>
                <input disabled={this.props.disabled} onChange={this.onFilterChange}/>
            </div>
        )
    }
}

InputFilmFilter.propTypes = {
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
}

export default InputFilmFilter;