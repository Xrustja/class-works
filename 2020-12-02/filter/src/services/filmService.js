import axios from 'axios';

class FilmService {
    constructor() {
    }

    static async getFilms() {
        return axios.get('https://ajax.test-danit.com/api/swapi/films')
    }
}

export default FilmService;