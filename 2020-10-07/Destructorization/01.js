const jsonFromServer = {
    name__from_Server: 'Sergey',
    saldo: 10000000,
    address: {
        "city": 'Kyiv',
        zip: '02152',
        country: 'UA'
    }

};
const {
    name__from_Server: name,
    some_magic_property: magic = 'default value',
    saldo: saldoForUI
} = jsonFromServer;

    //
    // const name = jsonFromServer.name__from_Server;
    // const magic = jsonFromServer.some_magic_property || 'default value'

