const jsonFromServer = {
    date: '02-03-2020',
    agent: 'FOP "Vasja"',
    saldo: 123,
    prefix: 'ДНК',
    number: '000000234567'
}

function getNumber({prefix = '__', number = '__'}) {
    return `${prefix}-${number}`;
}

console.log(getNumber(jsonFromServer));

function makeIndexForSaldo(index = 1.2, {saldo = 0}) {
    return saldo * index;
}

console.log(makeIndexForSaldo(1.5, jsonFromServer));
