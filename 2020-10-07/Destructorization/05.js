const jsonFromServer = {
    date: new Date(),
    prefix: 'DAN',
    number: 234567,
    size: 12,
    comment: '',
    name: 'Vasja'
}
function someFunc ({prefix ='FRE', number= 25, size= 10, ...rest}){
   let date1 = Date.now();
    let zeroNumber = size - String(number).length;

    const arr = [];
    for (let i = 0; i<zeroNumber;i++){
        arr.push('0');
    }
    let finalNumber = `${prefix}-${arr.join('')}${number} `;
    let date2 = Date.now();
 let time = date2-date1;
    return [ finalNumber, time, rest];
}

const [invoiceNumber,performance, rest] = someFunc(jsonFromServer);
// console.log(someFunc());
console.log(invoiceNumber);
console.log(performance);
console.log(rest);