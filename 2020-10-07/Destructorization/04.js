// const obj = {saldo: 10000, name:'Sergey'};

const {saldo} = {saldo: 10000, name:'Sergey'};

// const arr = Array.fill(150);
// const res = arr[145];

// const [first,second] = [1,2,3,4,5,15,26,20,125,157];
// const [first,,third] = [1,2,3,4,5,15,26,20,125,157];
const [first,,third, ...others] = [1,2,3,4,5,15,26,20,125,157];


function useState(initValue){
    let value = initValue;
    const changeVal = function (v){
        value = v;
    };
    return[value,changeVal];

};

const[val,setVal]=useState(100);

console.log('Val -->', val);
setVal(10000);

console.log('Val -->', val);

const {saldo, ...rest} = {saldo: 10000, name:'Sergey'};
