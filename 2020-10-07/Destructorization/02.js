function getName(firstName, lastName) {
    return `${firstName} ${lastName}`
}

const jsonFromServer = {
    firstName: 'Sergey',
    lastName: 'Tverdokhleb',
    saldo: 10000000,
    address: {
        "city": 'Kyiv',
        zip: '02152',
        country: 'UA'
    }
};

function getNameM1 ({firstName='__', lastName ='__'})
{
    return `${firstName} ${lastName}`;
}

console.log(getNameM1(jsonFromServer));