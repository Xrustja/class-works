(function dragDrop() {
    let coordX;
    let coordY;
    const dragEl = document.querySelectorAll('.drag-el')
    const dropZone = document.getElementById('wrapper')


    for (const el of dragEl) {
        el.draggable = true;
        el.addEventListener('dragstart', (e) => {
            e.dataTransfer.setData('text/html', 'dragstart')
            coordX = e.offsetX;
            coordY = e.offsetY;
            console.log('coordY = ', coordY)
            el.dataset.x = el.dataset.x || el.offsetLeft;
            el.dataset.y = el.dataset.y ||el.offsetTop;
            el.classList.toggle("active");
        })
    }

    dropZone.addEventListener('dragover', (e) => {
        e.preventDefault();

    });
    let zIndex = 0;
    dropZone.addEventListener('drop', (e) => {
        for (const el of dragEl) {

            if (el.classList.contains("active")) {
                el.style.position = 'relative';
                el.style.zIndex = ++zIndex;
                el.style.top = (e.pageY - coordY - +el.dataset.y) + 'px';
                console.log('e.pageY - coordY = ', e.pageY, '-', coordY)

                el.style.left = (e.pageX - coordX - +el.dataset.x) + 'px';
                console.log('e.pageX - coordX) = ', e.pageX, '-', coordX)
                el.classList.toggle("active");


            }
        }
    })
})
()