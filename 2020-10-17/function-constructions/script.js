// Функция-коструктор с большой буквы!!!

function Person({name,age,height}) {
    this.person_name = name;
    this.person_age = age;
    this.person_height = height;

}
// Part 1:
//  обьект типа Person
const p = new Person({
    name:'Serhii',
    age: 18,
    height: 182,
});
// просто обьект
const p1 = {
    name:'Max',
    age: 20,
    height: 170,
};

console.log ('P -->', p);


// Part 2
const arr = [
    new Person({
        name:'Christina',
        age: 18,
        height: 170,
    }),
    new Person({
        name:'Max',
        age: 20,
        height: 182,
    }),
    new Person({
    name:'Dima',
    age: 25,
    height: 190,
})
]
arr.forEach(person => {
    console.log(
        `${person.person_name} | ${person.person_age}`
    )});