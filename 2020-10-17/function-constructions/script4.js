const p1 = {
    name: 'P1',
    doSomething: function (param1, param2) {
        console.log(`Object with name - > ${this.name}`);
        console.log(param1, param2);
        return true;
    }
};

const p2 = {
    name: 'P2',
    doSomethingForP2: function() {
        console.log(`Method with name for p2 - > ${this.name}, ${param1}, ${param2}`);
        return true;
    }
};
console.log(p1.doSomething.apply(p2, [123, 345]));
console.log(p1.doSomething.call(p2, 123, 345));

