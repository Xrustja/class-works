function Person({name,age,height}) {
    this.person_name = name;
    this.person_age = age;
    this.person_height = height;
    /**
     * @desc Метод обьекта Person
     **/
    this.toString = function () {
        return `${this.person_name} | ${this.person_age}`;
    }
    this.call = function () {
        return `Calc for person --> "${this.person_name}"`
    }
}

const p = new Person({
    name:'Vasul',
    age: 25,
    height: 170,
})
// в стрелочных Ф-ЦИЯХ THIS  ссылается на глобальный обьект, а в обычных на обьект
const p1 = {
    person_name:'Max',
    person_age: 20,
    person_height: 170,
    toString: function() {
        return `${this.person_name} -> ${this.person_age}`;
    },
    doHust: function () {
        return `Do hust - > ${this.person_name}`
    }
};
// alert(p);
// alert(p1);

