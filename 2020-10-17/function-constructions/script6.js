function bind(context, fn) {
    return function (...args) {
        fn.apply(context, args);
    };
}
function Object1(name) {
    this.name = name;
    this.doSomething = function(p1, p2) {
        console.log(`o1 => ${this.name}, ${p1}, ${p2}`);
    }
}
function Object2(name) {
    this.name = name;
    this.doSomethingElse = function(p1, p2) {
        console.log(`o2 => ${this.name}.${p1}.${p2}`);
    }
}
let object1 = new Object1("1");
let object2 = new Object2("2");
let f1 = bind(object1, object2.doSomethingElse);
f1("a", "b");
let f2 = bind(object2, object1.doSomething);
f2("c", "d");