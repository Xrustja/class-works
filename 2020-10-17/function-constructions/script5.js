const p1 = {
    name: 'P1',
    age: 20,
    doSomething: function(param1, param2) {
        console.log(`Executing for object with name -> ${this.name}`);
        console.log(param1, param2);

        return true;
    }
};

const p2 = {
    name: 'P2',
    age: 1111111,
    doSomethingForP2: function() {
        console.log(`Method in P2 object -> ${this.name}`);
        return true;
    }
};


const newFunction = p1.doSomething.bind(p2);
console.log(newFunction('param1', 'param2'));
// p1.doSomething.call(p2, 123, 345);
