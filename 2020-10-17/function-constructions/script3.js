const methodName = 'doSomething';

const p1 = {
    name: 'P1',
    doSomething: function () {
        console.log(`Object with name - > ${this.name}`);
        return true;
    }
};

const p2 = {
    name: 'P2',
    doSomethingForP2: function (param1) {
        console.log(`Method with name for p2 - > ${this.name}, ${param1}`);
        return true;
    }
};
console.log(p2.doSomethingForP2.call(p1, 2000));
console.log(p2.doSomethingForP2(1111111));
p1[methodName].call(p2);
