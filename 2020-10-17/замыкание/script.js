function t(x) {
    let z = 10;
    return function (y) {
        return x+y+z;
    }
}
const firstFunction = t(5);
console.log(firstFunction(6));

// замыкания нужны для инкапсуляции, сокрытия данных