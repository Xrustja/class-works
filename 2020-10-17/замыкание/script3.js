function uniqueMaker() {
    let arr = [];
    return function () {
        if (arr.length === 5) {
            arr = [];
        }
        let a;
        do {
            a = Math.floor(Math.random() * 5);
        }
        while (arr.includes(a));
        arr.push(a);
        console.log(arr);
        return a;
    }
}

const func = uniqueMaker();
console.log(func());
console.log(func());
console.log(func());
console.log(func());
console.log(func());