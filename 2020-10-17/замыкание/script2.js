function counterMaker() {
    let counter = 0;
    return function () {
        return ++counter;
    }
}

const func = counterMaker();
console.log(func());
console.log(func());
console.log(func());
console.log(func());
